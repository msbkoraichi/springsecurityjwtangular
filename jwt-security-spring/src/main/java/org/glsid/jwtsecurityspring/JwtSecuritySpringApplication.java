package org.glsid.jwtsecurityspring;

import org.glsid.jwtsecurityspring.entities.AppRole;
import org.glsid.jwtsecurityspring.entities.AppUser;
import org.glsid.jwtsecurityspring.entities.Task;
import org.glsid.jwtsecurityspring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Stream;

@SpringBootApplication
public class JwtSecuritySpringApplication implements CommandLineRunner {
    @Autowired
    AccountService accountService;

    public static void main(String[] args) {
        SpringApplication.run(JwtSecuritySpringApplication.class, args);
    }

    @Bean
    public BCryptPasswordEncoder getBCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void run(String... args) throws Exception {
        Stream.of("A", "B", "C").forEach(t -> {
            accountService.saveTask(new Task(null, t));
        });
        accountService.saveUser(new AppUser(null, "user", "123", null));
        accountService.saveUser(new AppUser(null, "admin", "123", null));
        accountService.saveRole(new AppRole(null, "USER"));
        accountService.saveRole(new AppRole(null, "ADMIN"));
        accountService.addRoleToUser("user", "USER");
        accountService.addRoleToUser("admin", "USER");
        accountService.addRoleToUser("admin", "ADMIN");
        accountService.getAllTasks().forEach(System.out::println);
        System.out.println("-----------------------------------");
        accountService.getAllRoles().forEach(System.out::println);
        System.out.println("-----------------------------------");
        accountService.getAllUsers().forEach(System.out::println);
    }
}
