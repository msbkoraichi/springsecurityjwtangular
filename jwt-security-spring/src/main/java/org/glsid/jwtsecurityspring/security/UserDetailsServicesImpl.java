package org.glsid.jwtsecurityspring.security;

import org.glsid.jwtsecurityspring.entities.AppUser;
import org.glsid.jwtsecurityspring.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class UserDetailsServicesImpl implements UserDetailsService {
    @Autowired
    AccountService accountService;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        AppUser u=accountService.findUser(s);
        if(u==null)
            throw new UsernameNotFoundException(s);
        Collection<GrantedAuthority> authorities=new ArrayList<>();
        u.getRoles().forEach(r->{
            ((ArrayList<GrantedAuthority>) authorities).add(new SimpleGrantedAuthority(r.getRole()));
        });
        User springUser=new User(u.getUsername(),u.getPassword(),authorities);
        return springUser;
    }
}
