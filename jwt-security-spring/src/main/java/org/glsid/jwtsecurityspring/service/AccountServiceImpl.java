package org.glsid.jwtsecurityspring.service;

import org.glsid.jwtsecurityspring.dao.AppRoleRepository;
import org.glsid.jwtsecurityspring.dao.AppUserRepository;
import org.glsid.jwtsecurityspring.dao.TaskRepository;
import org.glsid.jwtsecurityspring.entities.AppRole;
import org.glsid.jwtsecurityspring.entities.AppUser;
import org.glsid.jwtsecurityspring.entities.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Autowired
    TaskRepository taskRepository;
    @Autowired
    AppUserRepository appUserRepository;
    @Autowired
    AppRoleRepository appRoleRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public Task saveTask(Task t) {
        return taskRepository.save(t);
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    @Override
    public AppUser saveUser(AppUser user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return appUserRepository.save(user);
    }

    @Override
    public List<AppUser> getAllUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public AppRole saveRole(AppRole role) {
        return appRoleRepository.save(role);
    }

    @Override
    public List<AppRole> getAllRoles() {
        return appRoleRepository.findAll();
    }

    @Override
    public void addRoleToUser(String username, String role) {
        AppUser u = null;
        AppRole r = null;
        u = appUserRepository.findByUsername(username);
        if (u == null)
            throw new UsernameNotFoundException(username);
        r = appRoleRepository.findByRole(role);
        if (r == null)
            throw new RuntimeException("couldn't find this role");
        u.getRoles().add(r);
    }

    @Override
    public AppUser findUser(String username) {
        return appUserRepository.findByUsername(username);
    }

    @Override
    public AppRole findRole(String role) {
        return appRoleRepository.findByRole(role);
    }
}
