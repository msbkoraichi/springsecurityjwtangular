package org.glsid.jwtsecurityspring.dao;

import org.glsid.jwtsecurityspring.entities.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppRoleRepository extends JpaRepository<AppRole,Long> {
    public AppRole findByRole(String role);
}
